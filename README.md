更新说明：

SequoiaDB 全新版本即将再度开源，敬请期待！


### SequoiaDB README


#### About Us

SequoiaDB is a distributed document database, based on a self-developed native distributed storage engine. SequoiaDB supports complete ACID transactions, with elastic scaling, high concurrency and high availability characteristics.

## The new version of SequoiaDB is about to be open-sourced again. Stay tuned!
